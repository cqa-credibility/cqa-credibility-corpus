========================================================================
Answers from the QatarLiving forum annotated for credibility.

July 25, 2017
========================================================================

This is the readme file for credibility annotations of answers from the 
QatarLiving forum. They are based on a subset of the unannotated data from the
CQA-QL-2016 corpus. The CQA-QL-2016 corpus [1] was originally created in 
the frame of the SemEval 2016 competition on community question answering [2,3].
The annotations for credibility were obtained by using CrowdFlower [4].

---
-1- CONTENTS
---

We provide the following files:


  * README.txt 
    this file
	

  * Credibility-CQA-QL-2017-train.xml
    Credibility-CQA-QL-2017-dev.xml
    Credibility-CQA-QL-2017-test.xml
	
	These are the files containing the annotations for credibility.
	Those files are following the SemEval 2016 file format (see 1-3 for details
	on the file format). For the credibility labels, the attribute RELC_CREDIBLE
	was added for the comment nodes. This attribute has the following values:
		** Credible - The answer was annotated as good and credible.
		**  NonCredible - The answer was annotated as good but not credible
		**  NA - The answer was annotated as Bad, i.e. it does not answer the 
		  question directly
		  
		  
  * CrowdFlower-annotations.csv
  
	Contains the annotations from CrowdFlower.
	
	
    
    
---    
-2- LICENSING
---

  These datasets are free for general research use.

---
-3- CITATION
---

You should use the following citation in your publications whenever using this 
resource:

@InProceedings{nakov-EtAl:2017:ranlp,
  author    = {Nakov, Preslav and
			Mihaylova, Tsvetomila and
			M\`arquez, Llu\'is and
			Shiroya, Yashkumar and
			Koychev, Ivan},
  title     = {Do Not Trust the Trolls: Predicting Credibility in 
			Community Question Answering Forums},
  booktitle = {Proceedings of RANLP '17},
  month     = {September},
  year      = {2017},
  address   = {Sofia, Bulgaria}
}

---
-4- CREDITS
---

Preslav Nakov, Qatar Computing Research Institute, HBKU, Qatar, pnakov@hbku.edu.qa
Tsvetomila Mihaylova, Sofia University "St. Kliment Ohridski", Bulgaria, tsvetomila.mihaylova@gmail.com
Llu�s M�rquez, Qatar Computing Research Institute, HBKU, Qatar, lmarquez@hbku.edu.qa
Yashkumar Shiroya, Purdue University, United States, yshiroya@purdue.edu
Ivan Koychev, Sofia University "St. Kliment Ohridski", Bulgaria, koychev@fmi.uni-sofia.bg


---
-5- REFERENCES
---

1. http://alt.qcri.org/semeval2016/task3/index.php?id=data-and-tools
2. http://alt.qcri.org/semeval2016/task3/
3. Preslav Nakov, Llu�s M�rquez, Alessandro Moschitti, Walid Magdy, Hamdy
Mubarak, Abed Alhakim Freihat, Jim Glass, and Bilal Randeree. 2016. SemEval-
2016 Task 3: Community Question Answering. In  Proceedings of SemEval '16, 
525-545. Association for Computational Linguistics, San Diego, CA.
4. https://www.crowdflower.com/